# Nanogame Template

This template is intended to be used as a base to develop community nanogames
for [Librerama](https://codeberg.org/Librerama/librerama). It comes with all
the project/export settings properly configured and contains all the basic
files necessary, as well as the their correct structure.

Instructions on how to use this template to develop a nanogame can be found in
[this wiki page](https://codeberg.org/Librerama/librerama/wiki/Developing-a-Community-Nanogame)
within the main repository.

## Licenses

Everything (with the exception of the contents inside the "nanogame_debugger"
directory) is under the
[CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/) license.

---

The contents of the "nanogame_debugger" directory are licensed as follows:

- Source code is under the
[GPL v3+](https://www.gnu.org/licenses/gpl-3.0.html) license.
- Media files are under the
[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) license.

The project is set to exclude those files when exporting.
